# grf-vimp

This project contains the R code of a variable importance algorithm for causal forests of the grf package (https://cran.r-project.org/package=grf).

The main file "vimp_causal_forests.R" stores the main function to compute the variable importance values for a causal forest fitted using the grf package.
The other files, "vimp_xp_1.R", "vimp_xp_2.R", and "vimp_xp_3.R" provide three experiments on simulated data to illustrate the importance algorithm in various settings.
